
export interface IArticleList {
    count: number,
    hasMore: boolean,
    items: IArticleItems[]
}

export interface IArticleItems{
    id: number,
    slug: string,
    srcSet: string,
    title: string,
    summary: string,
    author: string,
    pubDate: string
}