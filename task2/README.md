## Description
This is a Next.JS application

## Setup
1. Ensure Node is running on your local machine
2. Run `npm install` to install dependencies
3. Run  `npm run dev` to run application in dev mode
4. Run `npm run build` to generate buld distribution

