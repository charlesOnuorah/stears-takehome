import Head from 'next/head'
import Button from '../components/Buttons'
import styles from '../styles/auth.module.css'
import Link from 'next/link'
import { useSession, signIn } from "next-auth/react"
import {useRouter} from 'next/router'
import { useEffect } from 'react'
import { isAuthenticated } from '../utils'

export default function Home() {
    const router = useRouter();
  const { data: session } = useSession()

  useEffect(() => {
    if(session || isAuthenticated()){
      router.push('/home')
    }
      
  },[session])
  
  return (
    <div className={styles.container}>
      <Head>
        <title>LMM.co</title>
        <meta name="description" content="Lagos Money Media Company" />
        <link rel="icon" href="/favicon.ico" />
        
      </Head>

      <section className={styles.body}>
        <section className={styles.formWrapper}>
          <h4 className="title">Sign in to read Stears</h4>
          <div className={styles.content}>
            <Button onClick={() => signIn('google')} iconImage={'/google.svg'} text={'Sign in with Google'} />
            <Button onClick={() => signIn('facebook')} iconImage={'/google.svg'} text={'Sign in with Facebook'}/>
            <Link className="" href="/login">
              <a><Button text={'Sign in with username and password'} /></a>
            </Link>
          </div>
        </section>
      </section>
    </div>
  )
}
