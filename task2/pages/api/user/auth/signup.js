import { promises as fs } from 'fs'
import path from 'path'


export const userDBFilePath = path.join(process.cwd(),'data', 'users.json')

export default async (req, res) => {
  try{
    const {email, password, firstName} = req.query


    const usersDB = await fs.readFile(userDBFilePath, 'utf-8')
    const users = JSON.parse(usersDB)
    const user = users.find(item => item.email === email)

    if(user)
        return res.status(406).send({message: 'User already exists'});

    users.push({email, password, firstName})
    await fs.writeFile(userDBFilePath, JSON.stringify(users), 'utf8'); 
  
    res.status(201).send({message: 'Operation sucessful', token: new Date().getTime()});
  }catch(error){
    res.status(500).send({message: error?.message});
  }
};