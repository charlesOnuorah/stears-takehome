import moment from 'moment';
import Image from 'next/image';


/**
 * Renders an article card item
 * 
 * @param param0 
 * @returns 
 */
const ArticleCard = ({data}) => {
    return <div className='card'>
       <div className="card___image___wrapper">
         <a href="https://www.stears.co/" target="__blank">
            <Image src={data.srcSet} layout="fill" className="card___image" alt="author" />
        </a>
       </div>
        <h4 className="card___timer">
            <span>{new Date(data.pubDate).toDateString()}</span> <br />
            <span>{moment(data.pubDate).format('LTS')}</span>
        </h4>
        
        <h2 className="card___title">{data.title}</h2>
        <article className="card___summary">{data.summary}</article>
    </div>
}

export default ArticleCard;