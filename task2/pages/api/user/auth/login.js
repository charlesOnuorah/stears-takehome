import { promises as fs } from 'fs'
import { userDBFilePath } from './signup';


export default async (req, res) => {
  try{
    const {email, password} = req.query


    const usersDB = await fs.readFile(userDBFilePath, 'utf-8')
    const users = JSON.parse(usersDB)

      const user = users.find(user => user.email === email && user.password === password)

    if(!user)
        return res.status(404).send({message: 'Wrong Email/Password'})
  
    res.status(200).send({message: 'Operation sucessful',user,  token: new Date().getTime()});
  }catch(error){
    res.status(500).send({message: error?.message});
  }
};