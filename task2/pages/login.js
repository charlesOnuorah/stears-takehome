import Head from 'next/head'
import Button from '../components/Buttons'
import styles from '../styles/auth.module.css'
import Link from 'next/link'
import { useState, useEffect } from 'react'
import { validateEmail } from '../utils'
import Axios from 'axios'
import { useRouter } from 'next/router'

export default function Home() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const router = useRouter();

  const [emailValid, setEmailValid] = useState(true)
  const [emailValidationMessage, setEmailValidationMessage] = useState('')
  const [passwordValidationMessage, setPasswordValidationMessage] = useState(null)

  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)

  useEffect(() => {
    if(email.trim() === '' || validateEmail(email)){
        setEmailValid(true)
        setEmailValidationMessage('')
        return ;
    }
   else if(!validateEmail(email)){
        setEmailValidationMessage('Email is invalid')
        setEmailValid(false)
    }
        
  },[email])

  /**
   * 
   * @returns 
   */
  const handleLogin = async  () => {
    try{
        if(loading) return ;

        if(email.trim() === '' || !validateEmail(email)){
            setEmailValidationMessage('Email is invalid')
            setEmailValid(false)
            return ;
        }
        if(password.trim() === ''){
            setPasswordValidationMessage('password is required')
            return;

        }

        setPasswordValidationMessage(null)
        

        setError(null)
        setLoading(true)
        
       const response = await Axios.get(`/api/user/auth/login?email=${email}&password=${password}`)

       localStorage.setItem('user-token', response.data.token)
       setLoading(false)

       router.push('/home')
    }catch(error){
        setLoading(false)
        setError(error?.response?.data?.message)
    }
  }
  return (
    <div className={styles.container}>
      <Head>
        <title>LMM.co</title>
        <meta name="description" content="Lagos Money Media Company" />
        <link rel="icon" href="/favicon.ico" />
        
      </Head>

      <section className={styles.body}>
        <section className={styles.formWrapper}>
          <h4 className="title">Sign in to read Stears</h4>
          <div className={styles.content}>
            <div className="form__field">
                <input className="form___input" value={email}  placeholder="Email address" onChange={e => setEmail(e.target.value)} />
                {!emailValid && <span className="form-input-error">{emailValidationMessage}</span>}
            </div>
            <div className="form__field">
                <input value={password} type="password" onChange={e => setPassword(e.target.value)} className="form___input" placeholder="Password" />
                {passwordValidationMessage && <span className="form-input-error">{passwordValidationMessage}</span>}
            </div>
            
            <div className="button_field">
                <Button cls="button___dark" text={'Login'} onClick={handleLogin}/>
                {error && <span className="form-input-error">{error.toString()}</span>}
            </div>
            <Link href="/">
                <a><Button text={'Sign in with social'} /></a>
            </Link>

            <footer>
                Don’t have an account? <Link href="/signup">
                    <a>Sign up</a>
                </Link>
            </footer>
          </div>
        </section>
      </section>
    </div>
  )
}
