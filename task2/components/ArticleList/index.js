import ArticleCard from "../ArticleCard";


/**
 * Renders an article card list
 * 
 * @param param0 
 * @returns 
 */
const ArticleList  = ({data}) => {
    return <div className='cards'>
            {data.items.map((item, i) => <ArticleCard data={item} key={i} />)}
        </div>
}

export default ArticleList;