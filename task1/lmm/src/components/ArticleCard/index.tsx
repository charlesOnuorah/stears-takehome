import { FC } from "react";
import { IArticleItems } from "../../types/article-list.dto";
import momment from 'moment';

interface IProps{
    data: IArticleItems
}

/**
 * Renders an article card item
 * 
 * @param param0 
 * @returns 
 */
const ArticleCard: FC<IProps> = ({data}) => {
    return <div className='card'>
        <a href="https://www.stears.co/" target="__blank">
            <img src={data.srcSet} className="card___image" alt="author" />
        </a>
        <h4 className="card___timer">
            <span>{new Date(data.pubDate).toDateString()}</span> <br />
            <span>{momment(data.pubDate).format('LTS')}</span>
        </h4>
        
        <h2 className="card___title">{data.title}</h2>
        <article className="card___summary">{data.summary}</article>
    </div>
}

export default ArticleCard;