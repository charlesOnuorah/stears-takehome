import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import ArticleList from './components/ArticleList';
import ArticleCard from './components/ArticleCard';
import { ARTICLE_LIST } from './data/data';


test('renders App Component', () => {
  render(<App />);
  const mainElement = screen.getByRole('main');
  expect(mainElement).toBeInTheDocument();
});

test('renders Article List Component', () => {
  const {container} = render(<ArticleList data={ARTICLE_LIST} />);
  const cards = container.getElementsByClassName('card')
  expect(cards.length).toBe(10)
});

test('renders Article Card Component', () => {
  render(<ArticleCard data={ARTICLE_LIST.items[0]} />);
  const summaryText = screen.getByText(/Nigeria's president/i)
  
  expect(summaryText).toBeInTheDocument()
});

test('renders multiple Article Cards', () => {
  render(<ArticleList data={ARTICLE_LIST} />);
  const summaryTexts = screen.getAllByText(/Nigeria's president/i)
  
  expect(summaryTexts.length).toBeGreaterThan(3)
});
