# Description
 This is a coding homework test for Stears’ Senior Frontend Engineer role.

## Services

This project is made up of 3 services/application below

- Next.JS Application. path: `./task2`
- React Application. path: `./task1/lmm`
- HTML/CSS application. path: `./task1/html`

## Setup

1. Ensure you have Docker running on machine. link to [Docker](https://www.docker.com/get-started/) setup
2. Run `docker-compose build` to build the images
3. Run `docker-compose up -d` to run the containers
4. Run `docker-compose down` to stop the containers


### Application URL
Upon successful completion of step above, you can open the below URLS on your browser

- [HTML/CSS Application](http://localhost:3003)
- [React Application](http://localhost:3005)
- [Next.JS Application](http://localhost:3000)
