/**
 * Dummy data
 */
const ARTICLE_LIST = {
  count: 2,
  hasMore: false,
  items: [
    {
      id: 1,
      slug: "buharis-scorecard-any-hits-or-all-misses",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.1.jpeg",
      title: "Buhari’s scorecard: Any hits or all misses?",
      summary: "Nigeria's president has struggled with implementing many of his creative and solution-oriented promises.",
      author: "The Newsroom",
      pubDate: "2022-07-12T20:00:00.000Z"
    },
    {
      id: 2,
      slug: "how-does-lagos-state-spend-its-money",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.2.jpeg",
      title: "How does Lagos state spend its money?",
      summary: "Lagos is a megacity in Nigeria but by global standards, it’s not.",
      author: "Adesola Afolabi",
      pubDate: "2022-07-14T18:30:00.000Z"
    },
    {
      id: 1,
      slug: "buharis-scorecard-any-hits-or-all-misses",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.1.jpeg",
      title: "Buhari’s scorecard: Any hits or all misses?",
      summary: "Nigeria's president has struggled with implementing many of his creative and solution-oriented promises.",
      author: "The Newsroom",
      pubDate: "2022-07-12T20:00:00.000Z"
    },
    {
      id: 2,
      slug: "how-does-lagos-state-spend-its-money",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.2.jpeg",
      title: "How does Lagos state spend its money?",
      summary: "Lagos is a megacity in Nigeria but by global standards, it’s not.",
      author: "Adesola Afolabi",
      pubDate: "2022-07-14T18:30:00.000Z"
    },
    {
      id: 1,
      slug: "buharis-scorecard-any-hits-or-all-misses",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.1.jpeg",
      title: "Buhari’s scorecard: Any hits or all misses?",
      summary: "Nigeria's president has struggled with implementing many of his creative and solution-oriented promises.",
      author: "The Newsroom",
      pubDate: "2022-07-12T20:00:00.000Z"
    },
    {
      id: 2,
      slug: "how-does-lagos-state-spend-its-money",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.2.jpeg",
      title: "How does Lagos state spend its money?",
      summary: "Lagos is a megacity in Nigeria but by global standards, it’s not.",
      author: "Adesola Afolabi",
      pubDate: "2022-07-14T18:30:00.000Z"
    },
    {
      id: 1,
      slug: "buharis-scorecard-any-hits-or-all-misses",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.1.jpeg",
      title: "Buhari’s scorecard: Any hits or all misses?",
      summary: "Nigeria's president has struggled with implementing many of his creative and solution-oriented promises.",
      author: "The Newsroom",
      pubDate: "2022-07-12T20:00:00.000Z"
    },
    {
      id: 2,
      slug: "how-does-lagos-state-spend-its-money",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.2.jpeg",
      title: "How does Lagos state spend its money?",
      summary: "Lagos is a megacity in Nigeria but by global standards, it’s not.",
      author: "Adesola Afolabi",
      pubDate: "2022-07-14T18:30:00.000Z"
    },
    {
      id: 1,
      slug: "buharis-scorecard-any-hits-or-all-misses",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.1.jpeg",
      title: "Buhari’s scorecard: Any hits or all misses?",
      summary: "Nigeria's president has struggled with implementing many of his creative and solution-oriented promises.",
      author: "The Newsroom",
      pubDate: "2022-07-12T20:00:00.000Z"
    },
    {
      id: 2,
      slug: "how-does-lagos-state-spend-its-money",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.2.jpeg",
      title: "How does Lagos state spend its money?",
      summary: "Lagos is a megacity in Nigeria but by global standards, it’s not.",
      author: "Adesola Afolabi",
      pubDate: "2022-07-14T18:30:00.000Z"
    }
  ]
}

/**
 * When DOM is loaded, run the application
 */
document.addEventListener('DOMContentLoaded', function(){
    run()
})

/**
 * Creates and Returns a Card Item
 * @param data 
 * @returns 
 */
const makeCard = (data) => {
    const cardItemContainer = document.createElement('div')

    cardItemContainer.classList.add('card')

    const linkElement = document.createElement('a')

    const imageItem = document.createElement('img')
    linkElement.href = 'https://www.stears.co/'
    linkElement.append(imageItem)

    imageItem.src = data.srcSet
    imageItem.classList.add('card___image')

    const timerContainer = document.createElement('h4')

    timerContainer.classList.add('card___timer')

    cardItemContainer.append(linkElement)

    timerContainer.innerHTML = `<span>${new Date(data.pubDate).toDateString()}</span> <br />
            <span>${moment(data.pubDate).format('LTS')}</span>`

    cardItemContainer.append(timerContainer)

    const titleContainer = document.createElement('h2')

    titleContainer.classList.add('card___title')

    titleContainer.textContent = data.title;

    cardItemContainer.append(titleContainer)

    const summaryContainer = document.createElement('article')

    summaryContainer.classList.add('card___summary')

    summaryContainer.textContent = data.summary

    cardItemContainer.append(summaryContainer)

    return cardItemContainer;
}

/**
 * renders a card List
 */
const renderCardList = () => {

    const cardList = document.querySelector('main')

    const cardContainer = document.createElement('div')
    cardContainer.classList.add('cards')

    const articles = ARTICLE_LIST.items;

    for(let i = 0; i < articles.length; i++){
        const cardItem = makeCard(articles[i])

        cardContainer.append(cardItem)
    }

    cardList.append(cardContainer)
}

/**
 * Application entry point.
 */
const run = () => {
    renderCardList()
}

const clearList = () =>{
    const list = document.querySelector(".cards");

    while (list.hasChildNodes()) {
    list.removeChild(list.firstChild);
    }
}