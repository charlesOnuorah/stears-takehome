import { FC } from "react";
import { IArticleList } from "../../types/article-list.dto";
import ArticleCard from "../ArticleCard";

interface IProps{
    data: IArticleList
}

/**
 * Renders an article card list
 * 
 * @param param0 
 * @returns 
 */
const ArticleList : FC<IProps>  = ({data}) => {
    return <div className='cards'>
            {data.items.map((item, i) => <ArticleCard data={item} key={i} />)}
        </div>
}

export default ArticleList;