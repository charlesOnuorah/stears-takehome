import { signOut, useSession } from 'next-auth/react';
import Head from 'next/head'
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import ArticleList from '../components/ArticleList'
import Button from '../components/Buttons';
import { ARTICLE_LIST } from '../data/data'
import styles from '../styles/Home.module.css'
import { isAuthenticated, logout } from '../utils';


export default function Home() {
     const router = useRouter();
  const { data: session } = useSession()

  useEffect(() => {
    if(!session && !isAuthenticated()){
      router.push('/')
    }
      
  },[session])

  const handleLogout = () => {
    logout()
    signOut()
    
  }
  return (
    <div className={styles.container}>
      <Head>
        <title>LMM.co</title>
        <meta name="description" content="Lagos Money Media Company" />
        <link rel="icon" href="/favicon.ico" />
        
      </Head>
      <nav className="">
        <div className="logout-button-wrapper">
            <Button onClick={handleLogout} text={'Log out'} cls={'button___dark'} />
        </div>
      </nav>

      <main className={styles.main}>
        
        <ArticleList data={ARTICLE_LIST} />
      </main>
    </div>
  )
}
