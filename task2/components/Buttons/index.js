

const Button = ({text, iconImage, cls, onClick}) => {
    return <button onClick={onClick} className={`button ${cls}`}>
        {iconImage && <span className="button___icon">
                <img src={iconImage} alt="google" />
            </span>}
        {text}
    </button>
}

export default Button;