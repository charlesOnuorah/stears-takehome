import { IArticleList } from "../types/article-list.dto";

export const ARTICLE_LIST : IArticleList = {
  count: 2,
  hasMore: false,
  items: [
    {
      id: 1,
      slug: "buharis-scorecard-any-hits-or-all-misses",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.1.jpeg",
      title: "Buhari’s scorecard: Any hits or all misses?",
      summary: "Nigeria's president has struggled with implementing many of his creative and solution-oriented promises.",
      author: "The Newsroom",
      pubDate: "2022-07-12T20:00:00.000Z"
    },
    {
      id: 2,
      slug: "how-does-lagos-state-spend-its-money",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.2.jpeg",
      title: "How does Lagos state spend its money?",
      summary: "Lagos is a megacity in Nigeria but by global standards, it’s not.",
      author: "Adesola Afolabi",
      pubDate: "2022-07-14T18:30:00.000Z"
    },
    {
      id: 3,
      slug: "buharis-scorecard-any-hits-or-all-misses",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.1.jpeg",
      title: "Buhari’s scorecard: Any hits or all misses?",
      summary: "Nigeria's president has struggled with implementing many of his creative and solution-oriented promises.",
      author: "The Newsroom",
      pubDate: "2022-07-12T20:00:00.000Z"
    },
    {
      id: 4,
      slug: "how-does-lagos-state-spend-its-money",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.2.jpeg",
      title: "How does Lagos state spend its money?",
      summary: "Lagos is a megacity in Nigeria but by global standards, it’s not.",
      author: "Adesola Afolabi",
      pubDate: "2022-07-14T18:30:00.000Z"
    },
    {
      id: 5,
      slug: "buharis-scorecard-any-hits-or-all-misses",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.1.jpeg",
      title: "Buhari’s scorecard: Any hits or all misses?",
      summary: "Nigeria's president has struggled with implementing many of his creative and solution-oriented promises.",
      author: "The Newsroom",
      pubDate: "2022-07-12T20:00:00.000Z"
    },
    {
      id: 6,
      slug: "how-does-lagos-state-spend-its-money",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.2.jpeg",
      title: "How does Lagos state spend its money?",
      summary: "Lagos is a megacity in Nigeria but by global standards, it’s not.",
      author: "Adesola Afolabi",
      pubDate: "2022-07-14T18:30:00.000Z"
    },
    {
      id: 7,
      slug: "buharis-scorecard-any-hits-or-all-misses",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.1.jpeg",
      title: "Buhari’s scorecard: Any hits or all misses?",
      summary: "Nigeria's president has struggled with implementing many of his creative and solution-oriented promises.",
      author: "The Newsroom",
      pubDate: "2022-07-12T20:00:00.000Z"
    },
    {
      id: 8,
      slug: "how-does-lagos-state-spend-its-money",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.2.jpeg",
      title: "How does Lagos state spend its money?",
      summary: "Lagos is a megacity in Nigeria but by global standards, it’s not.",
      author: "Adesola Afolabi",
      pubDate: "2022-07-14T18:30:00.000Z"
    },
    {
      id: 9,
      slug: "buharis-scorecard-any-hits-or-all-misses",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.1.jpeg",
      title: "Buhari’s scorecard: Any hits or all misses?",
      summary: "Nigeria's president has struggled with implementing many of his creative and solution-oriented promises.",
      author: "The Newsroom",
      pubDate: "2022-07-12T20:00:00.000Z"
    },
    {
      id: 10,
      slug: "how-does-lagos-state-spend-its-money",
      srcSet: "https://gitlab.com/stears-interviews/frontend-engineer/-/raw/develop/task1.2.jpeg",
      title: "How does Lagos state spend its money?",
      summary: "Lagos is a megacity in Nigeria but by global standards, it’s not.",
      author: "Adesola Afolabi",
      pubDate: "2022-07-14T18:30:00.000Z"
    }
  ]
}
