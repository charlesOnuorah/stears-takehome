

const express = require("express");
const path = require("path");
const app = express();

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, "public")));

const PORT = process.env.PORT || 3003

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

// Start listening
app.listen(PORT, () => {
  console.log(`App is listening on port ${PORT}`);
});
