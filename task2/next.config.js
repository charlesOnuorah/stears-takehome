/** @type {import('next').NextConfig} */
require('dotenv').config()

const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ['gitlab.com'],
  },
}

module.exports = nextConfig
