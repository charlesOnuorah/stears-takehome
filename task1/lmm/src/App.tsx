
import './App.css';
import ArticleList from './components/ArticleList';
import { ARTICLE_LIST } from './data/data';

function App() {
  return (
    <main>
      <ArticleList data={ARTICLE_LIST} />
    </main>
  );
}

export default App;
